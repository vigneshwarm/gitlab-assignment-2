package com.themint.gitlab.runner.demo;

import org.reactivestreams.Publisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController

public class HelloController {

    @GetMapping("/hello")
    public Publisher<String> sayHello() {
        return Mono.just("Springboot Application is started ");
    }
}

