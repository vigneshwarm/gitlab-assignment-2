# springboot-app


Steps to run springboot application locally without Docker

1. Build the JAR File 

```
mvn clean install

```

2.  Build the Docker image

```
docker build -t docker_image .

```

3. Run the Docker app

```
docker run -d --name container_name -p 8080:8080 docker_image

```


NOTE: Make use of Artifacts while building jar file in the gitlab.

 



